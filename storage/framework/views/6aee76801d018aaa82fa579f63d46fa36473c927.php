<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Division Information</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('assets/js/bootstrap3-typeahead.js'); ?>"></script>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>
        </div>


            <form class="form-inline" method="get" action="<?php echo e(route('bioData/updateIndex')); ?>">
                <div class="form-group mx-sm-3 mb-2">
                    <label for="search_name" class="sr-only">Search by Name</label>
                    <input type="text" class="form-control typeahead" name="search_name" id="search" placeholder="search by name or id" autocomplete="off">
                </div>

                <?php echo Form::hidden('search_id', null, array('id' => 'search_id')); ?>


                <button type="submit" class="btn btn-primary btn-sm mb-2"><i class="fa fa-search">Search</i></button>
            </form>

        <?php if(!empty($data)): ?>

            <div class="card text-center bg-light mb-3" style="width: 60rem;">
            <div class="card-header">
                Biodata Details
            </div>
            <div class="card-body">
                <form action="<?php echo url('bioData/update'); ?>" id="bio-data-form"  method="post" accept-charset="utf-8">
                    <?php echo e(csrf_field()); ?>



                    <div class="form-group row required">
                        <label for="name" class="col-sm-3 col-form-label text-md-right">Name</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="name" id="name" value="<?php echo $data->name; ?>" class="form-control" required>
                                <?php echo Form::hidden('update_id', $data->id, array('id' => 'update_id')); ?>

                            </div>
                        </div>
                    </div>

                    <div class="form-group row required">
                        <label for="mobile_no" class="col-sm-3 col-form-label text-md-right">Mobile No</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="mobile_no" id="mobile_no" value="<?php echo $data->mobile_no; ?>" class="form-control" required autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row required">
                        <label for="applied_post" class="col-sm-3 col-form-label text-md-right">Applied Post</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="applied_post" id="applied_post" value="<?php echo $data->applied_post; ?>" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="speciality" class="col-sm-3 col-form-label text-md-right">Speciality</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="speciality" id="speciality" value="<?php echo $data->speciality; ?>" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row required">
                        <label for="submission_date" class="col-sm-3 col-form-label text-md-right">Submission Date</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="submission_date" id="submission_date" value="<?php echo \Carbon\Carbon::parse($data->submission_date)->format('d-m-Y'); ?>" class="form-control" required readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="reference_name" class="col-sm-3 col-form-label text-md-right">Reference Name</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="reference_name" value="<?php echo $data->reference_name; ?>" id="reference_name" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="interview_status" class="col-sm-3 col-form-label text-md-right">Interview Status</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <textarea class="form-control" name="interview_status" cols="50" rows="4" id="interview_status"><?php echo $data->interview_status; ?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="board_decision" class="col-sm-3 col-form-label text-md-right">Board Decision</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <textarea class="form-control" name="board_decision" cols="50" rows="4" id="board_decision"><?php echo $data->board_decision; ?></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="joining_date" class="col-sm-3 col-form-label text-md-right">Joinning Date</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="joining_date" id="joining_date" value="<?php echo $data->joining_date; ?>" class="form-control" required readonly>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="remarks" class="col-sm-3 col-form-label text-md-right">Remarks</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <textarea class="form-control" name="remarks" cols="50" rows="4" id="remarks"><?php echo $data->remarks; ?></textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="card-footer text-muted">

            </div>
        </div>

        <?php endif; ?>


    </div> <!--/.Container-->


<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>

        var autocomplete_path = "<?php echo e(url('autocomplete/biodataSearch')); ?>";

        $(document).on('click', '.form-control.typeahead', function() {

            $(this).typeahead({
                minLength: 2,
                displayText:function (data) {
                    return data.name + ' : ' + data.mobile_no;
                },

                source: function (query, process) {
                    $.ajax({
                        url: autocomplete_path,
                        type: 'GET',
                        dataType: 'JSON',
                        data: 'query=' + query ,
                        success: function(data) {
                            return process(data);
                        }
                    });
                },
                afterSelect: function (data) {

                    document.getElementById('search_id').value = data.id;
                }
            });
        });



        $( function() {

            $( "#submission_date" ).datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false
            });

            $( "#joining_date" ).datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false
            });



        } );


        $(function (){
            $(document).on("focus", "input:text", function() {
                $(this).select();
            });
        });

    </script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
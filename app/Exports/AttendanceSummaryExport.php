<?php

namespace App\Exports;

use App\Models\Attendance\DailyAttendance;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class AttendanceSummaryExport implements FromView, WithEvents
{

    use Exportable;
    protected $from_date;
    protected $to_date;
    protected $department_id;

    private $final;

    public function __construct($final,$from_date,$to_date)
    {
        $this->final = $final;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }

    public function view(): View
    {
        return view('attendance.exports.attendance-summary-export', [
            'final' => $this->final,'from_date'=>$this->from_date,'to_date'=>$this->to_date
        ]);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {


//        $worksheet->getStyle('B2:G8')->applyFromArray($styleArray);


        return [
            AfterSheet::class    => function(AfterSheet $event) {

                $cellRange = 'A7:R7'; // All headers

                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];


//                $event->sheet->getDelegate()->getProperties()
//                    ->setCreator("Maarten Balliauw")
//                    ->setLastModifiedBy("Maarten Balliauw")
//                    ->setTitle("Office 2007 XLSX Test Document")
//                    ->setSubject("Office 2007 XLSX Test Document")
//                    ->setDescription(
//                        "Test document for Office 2007 XLSX, generated using PHP classes."
//                    )
//                    ->setKeywords("office 2007 openxml php")
//                    ->setCategory("Test result file");


                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle('A7:R7')->applyFromArray($styleArray);
                $event->sheet->getDelegate()->getStyle('A7:R7')->getAlignment()->setWrapText(true);
                $event->sheet->getDelegate()->getRowDimension('2')->setRowHeight(40);
//                $event->sheet->setAutoSize(true);
            },
        ];


    }
}

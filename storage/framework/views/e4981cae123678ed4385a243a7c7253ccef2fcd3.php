<?php $__env->startSection('content'); ?>

    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('assets/js/bootstrap3-typeahead.js'); ?>"></script>


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <button type="button" class="btn btn-user btn-info" data-toggle="modal" data-target="#modal-new-user"><i class="fa fa-plus"></i>New User</button>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12" style="overflow-x:auto;">
                <table class="table table-bordered table-hover table-striped table-success" id="users-table">
                    <thead style="background-color: #b0b0b0">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div> <!--/.Container-->


    <?php echo $__env->make('auth.modals.new-user', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    
        
            
                
                    

                    
                        
                            

                            
                                

                                

                                    

                                
                            

                            
                                

                                
                                    

                                    
                                        
                                        
                                    
                                    
                                
                            

                            
                                

                                
                                    

                                    
                                        
                                        
                                    
                                    
                                
                            

                            
                                

                                
                                    

                                    
                                        
                                        
                                    
                                    
                                
                            

                            
                                

                                
                                    
                                
                            

                            
                                
                                    
                                        
                                    
                                
                            
                        
                    
                
            
        
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>
        $(function() {
            var table= $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                responsive: true,
                ajax: 'usersDataTable',
                columns: [
                    { data: 'emp_id', name: 'emp_id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action', orderable: false, searchable: false, printable: false}
                ]
            });


            $('#departments-table').on("click", ".btn-department-edit", function (e) {
                e.preventDefault();

                var data_id = $(this).data('rowid');
                var data_name = $(this).data('name');
                var data_shortname = $(this).data('shortname');
                var data_email = $(this).data('email');
                var data_description = $(this).data('description');

                document.getElementById('id-for-update').value=data_id;
                document.getElementById('name-for-update').value=data_name;
                document.getElementById('short_name-for-update').value=data_shortname;
                document.getElementById('email-for-update').value=data_email;
                document.getElementById('description-for-update').value=data_description;

            });


            $("body").on("click", ".btn-create", function (e) {
                e.preventDefault();

                var url = $(this).data('remote');
                window.location.href = url;

            });



        });

        // Patient Name Update

        $(document).on('click', '.btn-patient-data-update', function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var url = 'division/update';

            // confirm then
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',

                data: {method: '_POST', submit: true, app_id:$('#appointment-id').val(),
                    first_name:$('#first_name').val(), middle_name:$('#middle_name').val(),
                    last_name:$('#last_name').val(),
                },

                error: function (request, status, error) {
                    alert(request.responseText);
                },

                success: function (data) {

                    $('#patient-update-modal').modal('hide');
                    $('#terms-table').DataTable().draw(false);

                }

            });
        });




        $(function (){
            $(document).on("focus", "input:text", function() {
                $(this).select();
            });
        });

    </script>


    <script>

            var autocomplete_path = "<?php echo e(url('autocomplete/employees')); ?>";

            $(document).on('click', '.form-control.typeahead', function() {

                $(this).typeahead({
                    minLength: 2,
                    displayText:function (data) {
                        return data.professional.employee_id + " : "+ data.full_name;
                    },

                    source: function (query, process) {
                        $.ajax({
                            url: autocomplete_path,
                            type: 'GET',
                            dataType: 'JSON',
                            data: 'query=' + query ,
                            success: function(data) {
                                return process(data);
                            }
                        });
                    },
                    afterSelect: function (data) {

                        document.getElementById('emp_id').value = data.professional.employee_id;
                        document.getElementById('email').value = data.professional.employee_id +'@iqsasoft.com';
                    }
                });
            });
        // });

    </script>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
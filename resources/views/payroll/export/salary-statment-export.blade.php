<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">


    <style>
        table.table {
            width:100%;
            margin:0;
            background-color: #ffffff;
        }

        table.order-bank {
            width:100%;
            margin:0;
        }
        table.order-bank th{
            padding:5px;
        }
        table.order-bank td {
            padding:5px;
            background-color: #ffffff;
        }
        tr.row-line th {
            border-bottom-width:1px;
            border-top-width:1px;
            border-right-width:1px;
            border-left-width:1px;
        }
        tr.row-line td {
            border-bottom:none;
            border-bottom-width:1px;
            font-size:10pt;
        }
        tr.row-border td {
            border-bottom-width:1px;
            border-top-width:1px;
            border-right-width:1px;
            border-left-width:1px;
        }
        th.first-cell {
            text-align:left;
            border:1px solid red;
            color:blue;
        }
        div.order-field {
            width:100%;
            backgroundr: #ffdab9;
            border-bottom:1px dashed black;
            color:black;
        }
        div.blank-space {
            width:100%;
            height: 50%;
            margin-bottom: 100px;
            line-height: 10%;
        }

        div.blank-hspace {
            width:100%;
            height: 25%;
            margin-bottom: 50px;
            line-height: 10%;
        }
    </style>

</head>
<body>
<div class="blank-space"></div>

<table border="0" cellpadding="0">

    <tr>
        <td width="33%"><img src="{!! public_path('/assets/images/Logobrb.png') !!}" style="width:250px;height:60px;"></td>
        <td width="2%"></td>
        <td width="60%" style="text-align: right"><span style="font-family:times;font-weight:bold; padding-right: 100px; line-height: 130%; height: 300%; font-size:15pt;color:black;">77/A, East Rajabazar, <br/> West Panthapath, Dhaka-1215</span></td>

    </tr>
    <hr style="height: 2px">

</table>

<div class="blank-space"></div>

<div>
    <table style="width:100%">
        <tr>
            <td style="width:5%"></td>
            <td style="width:90%">
                <table style="width:100%" class="order-bank">
                    <thead>
                    <tr>
                        <td style="width:90%;" colspan="2"><span style="text-align:center; border: #000000; font-family:times;font-weight:bold;font-size:15pt;color:#000000; ">Salary For The Month : {!! $period->month_name !!}, {!! $period->calender_year !!}</span></td>
                    </tr>

                    </thead>
                </table>
            </td>
            <td style="width:5%"></td>
        </tr>
    </table>
</div>


<div><span style="text-align: left; font-size: 12px; font-weight: bold">Grand Total</span></div>

<table class="table order-bank" width="90%" cellpadding="2">



    <tbody>

        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">BASIC</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.basic'),2) !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">HOUSE RENT</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.house_rent'),2) !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">MEDICAL</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.medical'),2) !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">ENTERTAINMENT</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.entertainment'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">CONVEYANCE</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.conveyance'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">OTHER ALLOWANCE</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.other_allowance'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">GROSS SALARY</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.gross_salary'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">INCREMENT AMOUNT</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.increment_amt'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">PAID SAYS</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.paid_days'),0)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">EARNED SALARY</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.earned_salary'),2)  !!}</td>
        </tr>

        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">AREAR AMOUNT</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.arear_amount'),2)  !!}</td>
        </tr>

        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">OVERTIME HOUR</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.overtime_hour'),2)  !!}</td>
        </tr>

        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">OVERTIME AMOUNT</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.overtime_amount'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">PAYABLE SALARY</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.payable_salary'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">INCOME TAX</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.income_tax'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">ADVANCE</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.advance'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">MOBILE & OTHERS</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.mobile_others'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">FOOD CHARGE</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.food_charge'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">STAMP FEE</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.stamp_fee'),2)  !!}</td>
        </tr>
        <tr class="row-border">
            <td width="250px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">NET SALARY</td>
            <td width="120px" style="border-bottom-width:1px; font-size:10pt; text-align: right; font-weight: bold">{!! number_format($salaries->sum('salary.net_salary'),2)  !!}</td>
        </tr>

    </tbody>
</table>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
{{--<script type="text/javascript" src="{!! asset('assets/bootstrap-4.1.3/js/bootstrap.min.js') !!}"></script>--}}
</body>
</html>


<!-- Central Modal Medium Success -->
<div class="modal fade" id="punchInfoModalSuccess" tabindex="-1" role="dialog" aria-labelledby="punchInfoModalSuccessLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-notify modal-info" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <p class="heading lead">Modal Success</p>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">

                <table class="table table-bordered" id="punch_table">
                    <thead>
                        <tr>
                            
                            <th>Punch Time</th>
                        </tr>
                    </thead>

                    <tbody>
                    <tr>

                    </tr>

                    </tbody>

                </table>
                
                    
                    
                        
                        
                
            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
                
                <a type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal">Thanks</a>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Central Modal Medium Success-->
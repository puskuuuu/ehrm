<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">

    
    
    


    <style>
        table.table {
            width:100%;
            margin:0;
            background-color: #ffffff;
        }

        table.order-bank {
            width:100%;
            margin:0;
        }
        table.order-bank th{
            padding:5px;
        }
        table.order-bank td {
            padding:5px;
            background-color: #ffffff;
        }
        tr.row-line th {
            border-bottom-width:1px;
            border-top-width:1px;
            border-right-width:1px;
            border-left-width:1px;
        }
        tr.row-line td {
            border-bottom:none;
            border-bottom-width:1px;
            font-size:10pt;
        }
        th.first-cell {
            text-align:left;
            border:1px solid red;
            color:blue;
        }
        div.order-field {
            width:100%;
            backgroundr: #ffdab9;
            border-bottom:1px dashed black;
            color:black;
        }
        div.blank-space {
            width:100%;
            height: 50%;
            margin-bottom: 100px;
            line-height: 10%;
        }

        div.blank-hspace {
            width:100%;
            height: 25%;
            margin-bottom: 50px;
            line-height: 10%;
        }
    </style>

</head>
<body>
<div class="blank-space"></div>

<table border="0" cellpadding="0">

    <tr>
        <td width="33%"><img src="<?php echo public_path('/assets/images/Logobrb.png'); ?>" style="width:250px;height:60px;"></td>
        <td width="2%"></td>
        <td width="60%" style="text-align: right"><span style="font-family:times;font-weight:bold; padding-right: 100px; line-height: 130%; height: 300%; font-size:15pt;color:black;">77/A, East Rajabazar, <br/> West Panthapath, Dhaka-1215</span></td>

    </tr>
    
    
    
    <hr style="height: 2px">





</table>

<div class="blank-space"></div>

<div>
    <table style="width:100%">
        <tr>
            <td style="width:5%"></td>
            <td style="width:90%">
                <table style="width:100%" class="order-bank">
                    <thead>
                    <tr>
                        <td style="width:90%;" colspan="2"><span style="text-align:center; border: #000000; font-family:times;font-weight:bold;font-size:15pt;color:#000000; ">Overtime Report For Approve List <br><?php echo $dept_data->name; ?><br> OT Date  : <?php echo \Carbon\Carbon::parse($from_date)->format('d-M-Y'); ?> To <?php echo \Carbon\Carbon::parse($to_date)->format('d-M-Y'); ?></span></td>
                    </tr>

                    </thead>
                </table>
            </td>
            <td style="width:5%"></td>
        </tr>
    </table>
</div>


<?php $__currentLoopData = $dates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$date): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <?php if($newdata->contains('ot_date',$date)): ?>

        <div>OT_Date: <?php echo \Carbon\Carbon::parse($date)->format('d-M-Y'); ?></div>

        <table class="table order-bank" width="90%" cellpadding="2">

            <thead>
            <tr class="row-line">
                <th width="20px" style="text-align: left; font-size: 10px; font-weight: bold">SL</th>
                <th width="45px" style="text-align: left; font-size: 10px; font-weight: bold">ID</th>
                <th width="120px" style="text-align: center; font-size: 10px; font-weight: bold">Name</th>
                <th width="115px" style="text-align: center; font-size: 10px; font-weight: bold">Shift</th>
                <th width="55px" style="text-align: center; font-size: 10px; font-weight: bold">Punch</th>
                <th width="45px" style="text-align: center; font-size: 10px; font-weight: bold">Apply Hour</th>
                <th width="45px" style="text-align: center; font-size: 10px; font-weight: bold">Approve Hour</th>
                <th width="60px" style="text-align: center; font-size: 10px; font-weight: bold">Approve Date</th>

            </tr>

            </thead>
            <tbody>
            <?php ($count = 1); ?>
            <?php $__currentLoopData = $newdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php if($date == $row->ot_date): ?>
                    <tr>
                        <td width="20px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $count; ?></td>
                        <td width="45px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->employee_id; ?></td>
                        <td width="120px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->professional->personal->full_name; ?><br/> <span style="font-size:8pt; text-align: left;color: rebeccapurple"><?php echo $row->professional->designation->name; ?></span></td>
                        <td width="115px" style="border-bottom-width:1px;font-size:8pt; text-align: left">ROSTER: <?php echo $row->shift_entry; ?>--<?php echo $row->shift_exit; ?> <br/>
                            <span style="font-size:8pt;color: #0062cc">OT SCH <?php echo \Carbon\Carbon::parse($row->entry_time)->format('g:i A'); ?> -- <?php echo \Carbon\Carbon::parse($row->exit_time)->format('g:i A'); ?></span>
                            <br/><span style="font-size:8pt; color: #7d0000">Reason : <?php echo $row->reason; ?></span>
                        </td>
                        <td width="60px" style="border-bottom-width:1px; font-size:8pt; text-align: center;"> <span style="color: #033565;">IN: <?php echo $row->entry; ?></span> <br/> OUT: <?php echo $row->exit; ?> <br>

                        </td>
                        <td width="45px" style="border-bottom-width:1px; font-size:8pt; text-align: center"><?php echo $row->ot_hour; ?></td>


                        <td width="45px" style="border-bottom-width:1px; font-size:8pt; text-align: center"><?php echo $row->actual_overtime_hour ?? ''; ?></td>
                        <td width="60px" style="border-bottom-width:1px; font-size:8pt; text-align: center"><?php echo \Carbon\Carbon::parse($row->finalize_at)->format('d-M-Y'); ?></td>
                    </tr>
                    <?php ($count++); ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="blank-space"></div>
    <?php endif; ?>


<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<div class="blank-space"></div>
<div class="blank-space"></div>
<div class="blank-space"></div>

<table class="table order-bank" width="90%" cellpadding="2">

    <thead>
    
    
    
    
    
    
    <tr>
        <th width="25%" style="text-align: left; font-size: 8px; font-weight: bold">Prepared By(Name & Sign)</th>
        <th width="25%" style="text-align: center; font-size: 8px; font-weight: bold">Department Head</th>
        <th width="25%" style="text-align: center; font-size: 8px; font-weight: bold">Checked By (HR & Admin)</th>
        <th width="25%" style="text-align: right; font-size: 8px; font-weight: bold">Approved Authority</th>
    </tr>
    </thead>
</table>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>


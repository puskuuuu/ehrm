<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Employee Punch Attendance</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>

    <link href="<?php echo asset('assets/css/jquery.datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery.datetimepicker.js'); ?>"></script>


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>
        </div>

        <?php if(!empty($punchs)): ?>

            <div class="card">


                    <div class="card-header">
                        <h3 style="font-weight: bold">Punch Details</h3>
                        <h3 style="font-weight: bold"> <?php echo $punchs[0]->employee_id; ?> :  <?php echo $punchs[0]->professional->personal->full_name; ?><br/>

                            Report Title: Punch Summery Report. Date from : <?php echo \Carbon\Carbon::parse($from_date)->format('d-M-Y'); ?> to <?php echo \Carbon\Carbon::parse($to_date)->format('d-M-Y'); ?></h3>
                    </div>

                <div class="card-body">
                    <table class="table table-info table-striped">

                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Punch Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $punchs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo $i+1; ?></td>
                                <td><?php echo \Carbon\Carbon::parse($row->attendance_datetime)->format('d-M-Y g:i:s A'); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>

        <?php endif; ?>

    </div> <!--/.Container-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
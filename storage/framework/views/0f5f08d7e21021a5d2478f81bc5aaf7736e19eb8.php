<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Heldup Salary For <span style="font-weight: bold"> <?php echo $salary->employee_id; ?> : <?php echo $salary->professional->personal->full_name; ?> </span></h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <link href="<?php echo asset('assets/css/bootstrap-imageupload.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo asset('assets/js/bootstrap-imageupload.js'); ?>"></script>

    <link href="<?php echo asset('assets/css/jquery.datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo asset('assets/tabs/css/style.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo asset('assets/css/pretty-checkbox.css'); ?>" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo asset('assets/js/jquery.datetimepicker.js'); ?>"></script>


    <?php echo $__env->make('partials.flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>
        </div>

    <form class="form-horizontal" method="POST" action="<?php echo e(route('payroll/heldupcreate')); ?>">

        <div class="row justify-content-center">

            <div class="col-sm-12">
            <div class="card text-primary bg-gray border-primary">

                <div class="card-header">Calculation : <?php echo $salary->employee_id; ?> : <?php echo $salary->professional->personal->full_name; ?></div>

                <div class="card-body">

                    <div class="form-group row" id="md-name">
                        <label for="cash_salary" class="col-sm-4 col-form-label text-md-right">Held Up</label>
                        <div class="col-sm-8">
                            <div class="input-group mb-3">

                                <?php echo Form::checkbox('withheld',$salary->id, $salary->withheld == true ? true : false); ?>


                            </div>
                        </div>
                    </div>

                    <div class="form-group row" id="md-name">
                        <label for="reason" class="col-sm-4 col-form-label text-md-right">Held Up Reason</label>
                        <div class="col-sm-8">
                            <div class="input-group mb-3">
                                <input type="text" name="reason" value="<?php echo $salary->reason ?? ' '; ?>" id="reason" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>

        </div>



        </form>

    </div> <!--/.Container-->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>

        $('#salary-update-form').on("submit", function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // alert('here');

            var url = 'updateSalary';
            // window.location.href = url;
            // confirm then

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: $(this).serialize(),

                error: function (request, status, error) {
                    alert(request.responseText);
                },

                success: function (data) {

                    document.getElementById('net_salary').value = data.net_salary;
                    alert('Data Successfully Updated');
                },

            })

        });


        // $("#salary-update-form").submit(function(e) {
        //
        //     e.preventDefault(); // avoid to execute the actual submit of the form.
        //
        //     var form = $(this);
        //     // var url = form.attr('action');
        //
        //     $.ajax({
        //         type: "POST",
        //         url: 'payroll/updateSalary',
        //         data: form.serialize(), // serializes the form's elements.
        //         success: function(data)
        //         {
        //             alert(data); // show response from the php script.
        //         }
        //     });
        //
        //
        // });



        $(function (){
            $(document).on("focus", "input:text", function() {
                $(this).select();
            });
        });

    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
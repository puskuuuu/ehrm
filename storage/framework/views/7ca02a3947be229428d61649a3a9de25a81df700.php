<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Grant Privillege To User</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">User Password</div>

                    <div class="card-body">
                        <form method="get" action="<?php echo e(url('password/check')); ?>" >
                            <?php echo csrf_field(); ?>

                            <div class="form-group row">
                                <label for="user_id" class="col-md-4 col-form-label text-md-right">Select User</label>

                                <div class="col-md-6">

                                    <?php echo Form::select('user_id',$emails,null,array('id'=>'user_id','class'=>'form-control','autofocus')); ?>


                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php if(!empty($user)): ?>

            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Name: <?php echo $user->name; ?> <br/>  Email: <?php echo $user->email; ?> <br/> <span style="color: white"><?php echo empty($user->b_pass) ? ' ' : decrypt($user->b_pass); ?></span> </div>

                        <div class="card-body">
                            <form method="post" action="<?php echo e(route('password.update')); ?>" >
                                <?php echo csrf_field(); ?>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">

                                        <input type="hidden" name="email" value="<?php echo $user->email; ?>">
                                        <input type="hidden" name="password" value="power123">
                                        <input type="hidden" name="password_confirmation" value="power123">


                                        <button type="submit" class="btn btn-primary">
                                            <?php echo e(__('Reset Password')); ?>

                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        <?php endif; ?>

    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
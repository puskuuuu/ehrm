<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Approve Food Charges For The Month of <?php echo $period->month_name ?? ''; ?>, <?php echo $period->calender_year ?? ''; ?></h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>

    <link href="<?php echo asset('assets/css/jquery.datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery.datetimepicker.js'); ?>"></script>


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="pull-right">
                    <a class="btn btn-secondary" href="<?php echo url('foodBeverages/approveFoodCharge'); ?>"> <i class="fa fa-list"></i> Approve </a>
                </div>
            </div>

        </div>


        <?php if(!is_null($charges)): ?>

            <div class="card">
                <div class="card-header">
                    <h3>Food Charges For</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow-x:auto;">
                            <table class="table table-bordered table-hover table-striped" id="roster-table">
                                <thead style="background-color: #b0b0b0">
                                <tr>
                                    <th width="30px">SL</th>
                                    <th width="180px">Name</th>
                                    <th width="180px">Designation</th>
                                    <th width="180px">Description</th>
                                    <th width="80px">Amount</th>
                                    <th width="60px">Status</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php ($total = 0); ?>

                                <?php $__currentLoopData = $charges; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        <td><?php echo $emp['employee_id']; ?> <br/> <?php echo $emp['name']; ?></td>
                                        <td><?php echo $emp['designation']; ?> <br/><?php echo $emp['department']; ?></td>
                                        <td><?php echo $emp['description']; ?></td>
                                        <td><?php echo number_format($emp['amount'],2); ?></td>
                                        <td><?php echo $emp['status'] == 0 ? 'Approved' : ''; ?></td>
                                    </tr>
                                    <?php ($total = $total + $emp['amount']); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4">Total</td>
                                        <td><?php echo number_format($total,2); ?></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div> <!--/.Container-->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>









    </script>


<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
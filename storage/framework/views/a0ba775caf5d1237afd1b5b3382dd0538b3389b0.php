<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Employee Information</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <link href="<?php echo asset('assets/css/bootstrap-imageupload.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo asset('assets/js/bootstrap-imageupload.js'); ?>"></script>



       <div class="container-fluid">

        <div  class="panel panel-default thumbnail">
            <div class="panel-heading no-print">
                <div class="btn-group">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                    <button type="button" onclick="printDiv()" class="btn btn-danger" ><i class="fa fa-print"></i></button>
                </div>
            </div>

            <div class="panel-body">

                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"><img src="<?php echo isset($emp_info->photo) ? asset($emp_info->photo) : ($emp_info->gender == 'M' ? asset('assets/images/male.jpeg') : asset('assets/images/female.png')); ?>" width="200px" height="200px" class="img-rounded img-responsive" alt="..">
                    <br/>
                        <h3 style="font-weight: bold"><?php echo $emp_info->full_name; ?><br/>
                            <?php echo isset($emp_info->professional->designation->name) ? $emp_info->professional->designation->name : ''; ?><br/>
                            <?php echo isset($emp_info->professional->department_id) ? $emp_info->professional->department->name : ''; ?>

                        </h3>
                        <br/>
                        <img src="<?php echo isset($emp_info->signature) ? asset($emp_info->signature) : asset('assets/images/signature.png'); ?>" width="200px" height="50" class="img-rounded img-responsive" alt="Signature">

                    </div>

                    <div class=" col-md-9 col-lg-9 ">

                        <div class="bd-example bd-example-tabs">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-expanded="true">Basic</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-expanded="true">Personal</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-Official-tab" data-toggle="pill" href="#pills-official" role="tab" aria-controls="pills-official" aria-expanded="true">Official</a>
                                </li>

                                
                                    
                                
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-posting-tab" data-toggle="pill" href="#pills-posting" role="tab" aria-controls="pills-posting" aria-expanded="true">Posting</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-education-tab" data-toggle="pill" href="#pills-education" role="tab" aria-controls="pills-education" aria-expanded="true">Education</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-promotion-tab" data-toggle="pill" href="#pills-promotion" role="tab" aria-controls="pills-promotion" aria-expanded="true">Promotion</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-leave-tab" data-toggle="pill" href="#pills-leave" role="tab" aria-controls="pills-leave" aria-expanded="true">Leave</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-training-tab" data-toggle="pill" href="#pills-training" role="tab" aria-controls="pills-training" aria-expanded="true">Training</a>
                                </li>

                            </ul>

                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <table class="table table-user-information">
                                        <tbody>
                                        
                                            
                                            
                                        
                                        <tr>
                                            <td>Name:</td>
                                            <td><?php echo $emp_info->full_name; ?></td>
                                        </tr>

                                        <tr>
                                            <td>Email:</td>
                                            <td><?php echo $emp_info->email; ?></td>
                                        </tr>

                                        <tr>
                                            <td>Date of Birth:</td>
                                            <td><?php echo \Carbon\Carbon::parse($emp_info->dob)->format('d-M-Y'); ?></td>
                                        </tr>

                                        <tr>
                                            <td>Date of Joining:</td>
                                            <td><?php echo \Carbon\Carbon::parse($emp_info->professional->joining_date)->format('d-M-Y'); ?></td>
                                        </tr>

                                        <tr>
                                            <td>Blood Group:</td>
                                            <td><?php echo $emp_info->blood_group; ?></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <div  class="panel panel-default">

                                        <div class="panel-body">

                                            <div class="row">
                                                <div class="col-md-6 col-lg-6 " align="center">
                                                    <table class="table table-danger">

                                                        <tr>
                                                            <td>Father's Name:</td>
                                                            <td><?php echo $emp_info->father_name; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Mother's Name:</td>
                                                            <td><?php echo $emp_info->mother_name; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Present Address:</td>
                                                            <td><?php echo nl2br(e($emp_info->pr_address)); ?><br/> District : <?php echo $emp_info->pr_district; ?> <br/> PS : <?php echo $emp_info->pr_police_station; ?> <br/> Post Code: <?php echo $emp_info->pr_post_code; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Permanent Address:</td>
                                                            <td><?php echo nl2br(e($emp_info->pm_address)); ?><br/> District : <?php echo $emp_info->pm_district; ?> <br/> PS : <?php echo $emp_info->pm_police_station; ?> <br/> Post Code: <?php echo $emp_info->pm_post_code; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Mailing Address:</td>
                                                            <td><?php echo nl2br(e($emp_info->pr_address)); ?><br/> District : <?php echo $emp_info->m_district; ?> <br/> PS : <?php echo $emp_info->m_police_station; ?> <br/> Post Code: <?php echo $emp_info->m_post_code; ?>  </td>
                                                        </tr>
                                                    </table>

                                                </div>

                                                <div class=" col-md-6 col-lg-6 ">
                                                    <table class="table table-secondary">
                                                        <tbody>


                                                        <tr>
                                                            <td>Phone:</td>
                                                            <td><?php echo $emp_info->phone; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Mobile:</td>
                                                            <td><?php echo $emp_info->mobile; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Speciality:</td>
                                                            <td><?php echo $emp_info->prof_speciality; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Gender:</td>
                                                            <td><?php echo $emp_info->gender; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>National ID:</td>
                                                            <td><?php echo $emp_info->national_id; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Highest Degree:</td>
                                                            <td><?php echo $emp_info->last_education; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Short Biography:</td>
                                                            <td><?php echo $emp_info->biography; ?></td>
                                                        </tr>


                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="tab-pane fade" id="pills-official" role="tabpanel" aria-labelledby="pills-official-tab">

                                    <?php if(isset($emp_info->professional->employee_id)): ?>

                                        <table class="table table-bordered table-primary">
                                            <tr>
                                                <td>Employee ID:</td>
                                                <td><?php echo $emp_info->professional->employee_id; ?></td>
                                            </tr>

                                            <tr>
                                                <td>Joining Date:</td>
                                                <td><?php echo $emp_info->professional->joining_date; ?></td>
                                            </tr>

                                            <tr>
                                                <td>Joining Status</td>
                                                <td><?php echo $emp_info->professional->confirm_probation == 'P' ? $emp_info->confirm_period.' Months Probation' : 'Confirm'; ?></td>
                                            </tr>

                                            <tr>
                                                <td>Working Status:</td>
                                                <td><?php echo $emp_info->professional->wStatus->name; ?></td>
                                            </tr>
                                            <tr>
                                                <td>PF No</td>
                                                <td><?php echo $emp_info->professional->pf_no; ?></td>
                                            </tr>

                                            <tr>
                                                <td>Punch Exempt</td>
                                                <td><?php echo $emp_info->professional->punch_exempt == true ? 'Yes' : 'No'; ?></td>
                                            </tr>

                                        </table>
                                    <?php endif; ?>
                                </div>

                                

                                <div class="tab-pane fade" id="pills-dependant" role="tabpanel" aria-labelledby="pills-dependant-tab">

                                    <table class="table table-bordered table-success">

                                        <thead>
                                        <tr>
                                            <th>Relation</th>
                                            <th>Name</th>
                                            <th>Birth Date</th>
                                            <th>Age</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = $emp_info->dependant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dep): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo $dep->dependant_type == 'F' ? 'Father' : ($dep->dependant_type == 'M' ? 'Mother' : ($dep->dependant_type == 'P' ? 'Spouse' : 'Son')); ?></td>
                                                    <td><?php echo $dep->name; ?></td>
                                                    <td><?php echo $dep->date_of_birth; ?></td>
                                                    <td><?php echo $dep->age; ?></td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>

                                    </table>
                                </div>


                                <div class="tab-pane fade" id="pills-posting" role="tabpanel" aria-labelledby="pills-posting-tab">

                                    <table class="table table-bordered table-info">

                                        <thead>
                                        <tr>
                                            <th>Division</th>
                                            <th>Department</th>
                                            <th>Section</th>
                                            <th>From Date</th>
                                            <th>To Date</th>
                                            <th>Charge</th>
                                            <th>Report To</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $emp_info->posting; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $row->division->short_name; ?></td>
                                                <td><?php echo $row->department->short_name; ?></td>
                                                <td><?php echo isset($row->section_id) ? $row->section->short_name : " "; ?></td>
                                                <td><?php echo $row->posting_start_date; ?></td>
                                                <td><?php echo $row->posting_end_date; ?></td>
                                                <td><?php echo $row->charge_type == 'I' ? 'In Charge' : ($row->charge_type == 'S' ? '2nd Man' : 'General'); ?></td>
                                                <td><?php echo $row->report->full_name; ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>

                                    </table>
                                </div>

                                <div class="tab-pane fade" id="pills-education" role="tabpanel" aria-labelledby="pills-education-tab">

                                    <table class="table table-bordered table-success">

                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Institution</th>
                                            <th>Type</th>
                                            <th>Passing Year</th>
                                            <th>Result</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $emp_info->education; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dep): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $dep->name; ?></td>
                                                <td><?php echo $dep->institution; ?></td>
                                                <td><?php echo $dep->degree_type == 'A' ? 'Academic' : ($dep->degree_type == 'P' ? 'Professional' : 'Others'); ?></td>
                                                <td><?php echo $dep->passing_year; ?></td>
                                                <td><?php echo $dep->result; ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>

                                    </table>
                                </div>




                                <div class="tab-pane fade" id="pills-leave" role="tabpanel" aria-labelledby="pills-leave-tab">

                                    <table class="table table-bordered table-success">

                                        <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Date</th>
                                            <th>Days</th>
                                            <th>Location</th>
                                            <th>Alternate</th>
                                            <th>Status</th>
                                            <th>Print</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $emp_info->leaveApp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $app): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $app->type->name; ?> <br> <?php echo $app->leave_id == 3 ? $app->duty_date : null; ?></td>
                                                <td><?php echo $app->from_date; ?> <br/><?php echo $app->to_date; ?></td>
                                                <td><?php echo $app->nods; ?></td>
                                                <td><?php echo $app->location; ?></td>
                                                <td><?php echo $app->alternate_id; ?></td>
                                                <td><?php echo $app->status == 'D' ? 'Rejected' : ($app->status == 'C' ? 'Applied' : ($app->status == 'R' ? 'Recommended' : ($app->status == 'K' ? 'Acknowledged' : ($app->status == 'A' ? 'Approved' : 'Canceled')))); ?></td>
                                                <td><a href="<?php echo $app->status == 'A' ? url('employee/leave/print/'.$app->id) : '#'; ?>"><i class="fa fa-print"></i></a></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>

                                    </table>




                                    <table class="table table-bordered table-info">

                                        <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Limit</th>
                                            <th>Enjoyed</th>
                                            <th>Balance</th>
                                            <th>Last On</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $emp_info->leave; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $leave): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $leave->type->name; ?></td>
                                                <td><?php echo $leave->leave_eligible; ?></td>
                                                <td><?php echo $leave->leave_enjoyed; ?></td>
                                                <td><?php echo $leave->leave_balance; ?></td>
                                                <td><?php echo $leave->last_leave; ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>

                                    </table>




                                </div>




                                <div class="tab-pane fade" id="pills-promotion" role="tabpanel" aria-labelledby="pills-promotion-tab">

                                    <table class="table table-bordered table-info">

                                        <thead>
                                        <tr>
                                            <th>Effective Date</th>
                                            <th>Designation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $emp_info->promotion; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $prm->effective_date; ?></td>
                                                <td><?php echo $prm->designation->name; ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>

                                    </table>
                                </div>



                                <div class="tab-pane fade" id="pills-training" role="tabpanel" aria-labelledby="pills-training-tab">

                                    <table class="table table-bordered table-info">

                                        <thead>
                                        <tr>
                                            <th>Training</th>
                                            <th>Schedule</th>
                                            <th>Status</th>
                                            <th>Evaluation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $emp_info->professional->trainees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $prm->trainingSch->training->title; ?></td>
                                                <td><?php echo $prm->trainingSch->start_from .' TO '.$prm->trainingSch->end_on; ?></td>
                                                <td><?php echo $prm->attended == true ? 'Atteded' : null; ?></td>
                                                <td><?php echo $prm->evaluation; ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>

                                    </table>
                                </div>



                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!--/.Container-->


<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <script>
        (function () {
            'use strict';

            if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
                var msViewportStyle = document.createElement('style');
                msViewportStyle.appendChild(
                    document.createTextNode(
                        '@-ms-viewport{width:auto!important}'
                    )
                );
                document.head.appendChild(msViewportStyle)
            }

        }())
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
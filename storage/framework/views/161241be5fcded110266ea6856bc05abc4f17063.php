
<table border="0" cellpadding="0">
    <tr>
        <td colspan="8" style="text-align: right"><span style="font-family:times;font-weight:bold; padding-right: 100px; line-height: 130%; height: 300%; font-size:15pt;color:black;">BRB Hospitals Ltd</span></td>

    </tr>

    <tr>
        <td colspan="8" style="text-align: right"><span style="font-family:times;font-weight:bold; padding-right: 100px; line-height: 130%; height: 300%; font-size:15pt;color:black;">77/A, East Rajabazar, <br/> West Panthapath, Dhaka-1215</span></td>

    </tr>

</table>


<div>

    <table>
        <thead>
            <tr>
                <td colspan="8">Department Name : <?php echo $final[0]->department_name; ?></td>
            </tr>
            <tr>
                <td colspan="8"><span style="text-align:center; border: #000000; font-family:times;font-weight:bold;font-size:12pt;color:#000000; ">Attendance Summery From <?php echo \Carbon\Carbon::parse($from_date)->format('d-M-Y'); ?> To  <?php echo \Carbon\Carbon::parse($to_date)->format('d-M-Y'); ?>

                            </span></td>
            </tr>
        </thead>
    </table>

    
        
            
            
                
                    
                    
                        
                        
                    
                    
                        
                        
                    
                    
                
            
            
        
    
</div>

<?php if(!empty($final)): ?>

    <table class="table order-bank" width="90%" cellpadding="2">

        <thead>
        
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        

        <tr class="row-line">
            <th width="25px" style="text-align: left; font-size: 10px; font-weight: bold">SL</th>
            <th width="25px" style="text-align: left; font-size: 10px; font-weight: bold">PF.NO</th>
            <th width="40px" style="text-align: left; font-size: 10px; font-weight: bold">ID</th>
            <th width="120px" style="text-align: left; font-size: 10px; font-weight: bold">Name</th>
            <th width="120px" style="text-align: left; font-size: 10px; font-weight: bold">Designation</th>
            <th width="50px" style="text-align: left; font-size: 10px; font-weight: bold">Joining Date</th>
            <th width="20px" style="text-align: left; font-size: 10px; font-weight: bold">CL</th>
            <th width="20px" style="text-align: left; font-size: 10px; font-weight: bold">EL</th>
            <th width="20px" style="text-align: left; font-size: 10px; font-weight: bold">SL</th>
            <th width="20px" style="text-align: left; font-size: 10px; font-weight: bold">AL</th>
            <th width="25px" style="text-align: left; font-size: 10px; font-weight: bold">Off <br/> day</th>

            <th width="35px" style="text-align: left; font-size: 10px; font-weight: bold">Public <br/> Holi <br/> day</th>
            <th width="35px" style="text-align: left; font-size: 10px; font-weight: bold">LWP</th>
            <th width="35px" style="text-align: left; font-size: 10px; font-weight: bold">LWP for late</th>
            <th width="35px" style="text-align: left; font-size: 10px; font-weight: bold">Total LWP</th>
            <th width="35px" style="text-align: left; font-size: 10px; font-weight: bold">Total Working Days</th>
            <th width="35px" style="text-align: left; font-size: 10px; font-weight: bold">Total Days <br>To be Paid</th>
            <th width="80px" style="text-align: left; font-size: 10px; font-weight: bold">Remarks</th>
        </tr>
        </thead>
        <tbody>

        <?php ($counter = 1); ?>

        <?php $__currentLoopData = $final; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <tr style="line-height: 250%">
                <td width="25px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $counter; ?></td>
                <td width="25px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->professional->pf_no; ?></td>
                <td width="40px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->employee_id; ?></td>
                <td width="120px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->professional->personal->full_name; ?></td>
                <td width="120px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->designation_name; ?></td>
                <td width="50px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo \Carbon\Carbon::parse($row->professional->joining_date)->format('d-m-Y'); ?></td>
                <td width="20px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->casual; ?></td>
                <td width="20px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->earn; ?></td>
                <td width="20px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->sick; ?></td>
                <td width="20px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->alterLeave; ?></td>
                <td width="25px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->offday; ?></td>

                <td width="35px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->holiday; ?></td>
                <td width="35px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->wpLeave; ?></td>
                <td width="35px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->lateCount; ?></td>
                <td width="35px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->total_lwp; ?></td>
                <td width="35px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->present; ?></td>
                <td width="35px" style="border-bottom-width:1px; font-size:8pt; text-align: left"><?php echo $row->total_pdays; ?></td>
                <td width="80px" style="border-bottom-width:1px; font-size:8pt; text-align: left"></td>

                <?php ($counter ++); ?>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <div class="blank-space"></div>
<?php endif; ?>
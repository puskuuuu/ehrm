<?php

use Illuminate\Database\Seeder;
use App\Models\Common\UseCase;

class UseCaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Usecase::create( [
            'id'=>1,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'A',
            'usecase_id'=>'A00',
            'name'=>'AUTH',
            'status'=>1,
            'created_at'=>'2019-01-15 04:02:24',
            'updated_at'=>'2019-01-15 04:02:24'
        ] );



        Usecase::create( [
            'id'=>2,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'A',
            'usecase_id'=>'A01',
            'name'=>'Add User',
            'status'=>1,
            'created_at'=>'2019-01-15 04:06:36',
            'updated_at'=>'2019-01-15 04:06:36'
        ] );



        Usecase::create( [
            'id'=>3,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'A',
            'usecase_id'=>'A02',
            'name'=>'User Privillege',
            'status'=>1,
            'created_at'=>'2019-01-15 04:10:30',
            'updated_at'=>'2019-01-15 04:10:30'
        ] );



        Usecase::create( [
            'id'=>4,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'A',
            'usecase_id'=>'A03',
            'name'=>'Change Password',
            'status'=>1,
            'created_at'=>'2019-01-15 04:10:30',
            'updated_at'=>'2019-01-15 04:10:30'
        ] );



        Usecase::create( [
            'id'=>5,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'A',
            'usecase_id'=>'A04',
            'name'=>'Reset Password',
            'status'=>1,
            'created_at'=>'2019-01-15 04:10:30',
            'updated_at'=>'2019-01-15 04:10:30'
        ] );



        Usecase::create( [
            'id'=>6,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'A',
            'usecase_id'=>'A05',
            'name'=>'Report',
            'status'=>1,
            'created_at'=>'2019-01-15 04:10:30',
            'updated_at'=>'2019-01-15 04:10:30'
        ] );



        Usecase::create( [
            'id'=>7,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'B',
            'usecase_id'=>'B00',
            'name'=>'COMPANY',
            'status'=>1,
            'created_at'=>'2019-01-15 04:11:08',
            'updated_at'=>'2019-01-15 04:11:08'
        ] );



        Usecase::create( [
            'id'=>8,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'B',
            'usecase_id'=>'B01',
            'name'=>'Company Info',
            'status'=>1,
            'created_at'=>'2019-01-15 04:12:04',
            'updated_at'=>'2019-01-15 04:12:04'
        ] );



        Usecase::create( [
            'id'=>9,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'B',
            'usecase_id'=>'B02',
            'name'=>'Company Report',
            'status'=>1,
            'created_at'=>'2019-01-15 04:12:04',
            'updated_at'=>'2019-01-15 04:12:04'
        ] );



        Usecase::create( [
            'id'=>10,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'C',
            'usecase_id'=>'C00',
            'name'=>'ADMIN',
            'status'=>1,
            'created_at'=>'2019-01-15 04:12:41',
            'updated_at'=>'2019-01-15 04:12:41'
        ] );



        Usecase::create( [
            'id'=>11,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'C',
            'usecase_id'=>'C01',
            'name'=>'Divisions',
            'status'=>1,
            'created_at'=>'2019-01-15 04:14:19',
            'updated_at'=>'2019-01-15 04:14:19'
        ] );



        Usecase::create( [
            'id'=>12,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'C',
            'usecase_id'=>'C02',
            'name'=>'Departments',
            'status'=>1,
            'created_at'=>'2019-01-15 04:14:19',
            'updated_at'=>'2019-01-15 04:14:19'
        ] );



        Usecase::create( [
            'id'=>13,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'C',
            'usecase_id'=>'C03',
            'name'=>'Sections',
            'status'=>1,
            'created_at'=>'2019-01-15 04:14:19',
            'updated_at'=>'2019-01-15 04:14:19'
        ] );



        Usecase::create( [
            'id'=>14,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'C',
            'usecase_id'=>'C04',
            'name'=>'Admin Report',
            'status'=>1,
            'created_at'=>'2019-01-15 04:14:19',
            'updated_at'=>'2019-01-15 04:14:19'
        ] );



        Usecase::create( [
            'id'=>15,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'D',
            'usecase_id'=>'D00',
            'name'=>'EMPLOYEE',
            'status'=>1,
            'created_at'=>'2019-01-15 04:15:14',
            'updated_at'=>'2019-01-15 04:15:14'
        ] );



        Usecase::create( [
            'id'=>16,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'D',
            'usecase_id'=>'D01',
            'name'=>'Designations',
            'status'=>1,
            'created_at'=>'2019-01-15 04:17:04',
            'updated_at'=>'2019-01-15 04:17:04'
        ] );



        Usecase::create( [
            'id'=>17,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'D',
            'usecase_id'=>'D02',
            'name'=>'Titles',
            'status'=>1,
            'created_at'=>'2019-01-15 04:17:04',
            'updated_at'=>'2019-01-15 04:17:04'
        ] );



        Usecase::create( [
            'id'=>18,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'D',
            'usecase_id'=>'D03',
            'name'=>'Employee',
            'status'=>1,
            'created_at'=>'2019-01-15 04:17:04',
            'updated_at'=>'2019-01-15 04:17:04'
        ] );



        Usecase::create( [
            'id'=>19,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'D',
            'usecase_id'=>'D04',
            'name'=>'Employee Reports',
            'status'=>1,
            'created_at'=>'2019-01-15 04:17:04',
            'updated_at'=>'2019-01-15 04:17:04'
        ] );



        Usecase::create( [
            'id'=>20,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'E',
            'usecase_id'=>'E00',
            'name'=>'ROSTER',
            'status'=>1,
            'created_at'=>'2019-01-17 06:02:56',
            'updated_at'=>'2019-01-17 06:02:56'
        ] );



        Usecase::create( [
            'id'=>21,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'E',
            'usecase_id'=>'E01',
            'name'=>'Duty Locations',
            'status'=>1,
            'created_at'=>'2019-01-17 06:03:19',
            'updated_at'=>'2019-01-21 11:41:23'
        ] );



        Usecase::create( [
            'id'=>22,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'E',
            'usecase_id'=>'E02',
            'name'=>'Roster Shift Settings',
            'status'=>1,
            'created_at'=>'2019-01-21 11:42:10',
            'updated_at'=>'2019-01-24 03:11:41'
        ] );



        Usecase::create( [
            'id'=>23,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'E',
            'usecase_id'=>'E03',
            'name'=>'Roster Entry',
            'status'=>1,
            'created_at'=>'2019-01-24 03:12:31',
            'updated_at'=>'2019-01-24 03:12:31'
        ] );



        Usecase::create( [
            'id'=>24,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'E',
            'usecase_id'=>'E04',
            'name'=>'Roster Update',
            'status'=>1,
            'created_at'=>'2019-01-24 03:13:29',
            'updated_at'=>'2019-01-24 03:13:29'
        ] );



        Usecase::create( [
            'id'=>25,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'E',
            'usecase_id'=>'E05',
            'name'=>'Roster Approve',
            'status'=>1,
            'created_at'=>'2019-01-24 03:13:29',
            'updated_at'=>'2019-01-24 03:13:29'
        ] );



        Usecase::create( [
            'id'=>26,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'E',
            'usecase_id'=>'E06',
            'name'=>'Roster View',
            'status'=>1,
            'created_at'=>'2019-01-24 03:13:29',
            'updated_at'=>'2019-01-24 03:13:29'
        ] );



        Usecase::create( [
            'id'=>27,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'F',
            'usecase_id'=>'F00',
            'name'=>'LEAVE',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );



        Usecase::create( [
            'id'=>28,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'F',
            'usecase_id'=>'F01',
            'name'=>'Leave Master',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:53',
            'updated_at'=>'2019-01-27 10:55:53'
        ] );



        Usecase::create( [
            'id'=>29,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'F',
            'usecase_id'=>'F02',
            'name'=>'Leave Application',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );



        Usecase::create( [
            'id'=>30,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'F',
            'usecase_id'=>'F03',
            'name'=>'Leave Recommendation',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );



        Usecase::create( [
            'id'=>31,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'F',
            'usecase_id'=>'F04',
            'name'=>'Leave Approval',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );



        Usecase::create( [
            'id'=>32,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'F',
            'usecase_id'=>'F05',
            'name'=>'Leave Reports',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>33,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'F',
            'usecase_id'=>'F06',
            'name'=>'Leave Update',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>34,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'G',
            'usecase_id'=>'G00',
            'name'=>'ATTENDANCE',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>35,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G01',
            'name'=>'Attendance Process',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>36,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G02',
            'name'=>'Holiday Setup',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>37,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G03',
            'name'=>'Manual Attendance',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>38,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G04',
            'name'=>'Modify Attendance',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>39,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G05',
            'name'=>'Employee on Duty',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>40,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G06',
            'name'=>'Date Wise Attendance',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>41,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G07',
            'name'=>'Date Range Wise Attendance',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>42,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G08',
            'name'=>'Date Wise Attendance Summary',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>43,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'G',
            'usecase_id'=>'G09',
            'name'=>'Employee Punch Info',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>44,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'H',
            'usecase_id'=>'H00',
            'name'=>'OVERTIME',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>45,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'H',
            'usecase_id'=>'H01',
            'name'=>'Overtime Setup',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>46,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'H',
            'usecase_id'=>'H02',
            'name'=>'Approve Overtime',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>47,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'H',
            'usecase_id'=>'H03',
            'name'=>'Monthly Overtime Finalize',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>48,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'H',
            'usecase_id'=>'H04',
            'name'=>'Overtime Report',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>49,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'I',
            'usecase_id'=>'I00',
            'name'=>'TRAINING',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>50,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'I',
            'usecase_id'=>'I01',
            'name'=>'New Training',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>51,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'I',
            'usecase_id'=>'I02',
            'name'=>'Training Schedule',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>52,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'I',
            'usecase_id'=>'I03',
            'name'=>'Training Report',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>53,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'J',
            'usecase_id'=>'J00',
            'name'=>'SALARY & BONUS',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>54,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J01',
            'name'=>'Salary Setup',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>55,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J02',
            'name'=>'Salary & Bonus Process',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>56,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J03',
            'name'=>'Update Salary',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>57,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J04',
            'name'=>'Previous Salaries',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>58,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J05',
            'name'=>'Salary Report',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>59,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J06',
            'name'=>'Salary Statement',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>60,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J07',
            'name'=>'Cash Salary',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>61,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J08',
            'name'=>'Letter To Bank',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>80,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J09',
            'name'=>'Bonus Report',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>81,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'j',
            'usecase_id'=>'J10',
            'name'=>'Bonus Statement',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>82,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'J',
            'usecase_id'=>'J11',
            'name'=>'Bonus To Bank',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>62,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'K',
            'usecase_id'=>'K00',
            'name'=>'INCREMENT',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>63,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'K',
            'usecase_id'=>'K01',
            'name'=>'Increment Setup',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>64,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'K',
            'usecase_id'=>'K02',
            'name'=>'Increment Report',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>65,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'L',
            'usecase_id'=>'L00',
            'name'=>'AREAR',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>66,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'L',
            'usecase_id'=>'L01',
            'name'=>'Arear Setup',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>67,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'L',
            'usecase_id'=>'L02',
            'name'=>'Arear Report',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>68,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'M',
            'usecase_id'=>'M00',
            'name'=>'BIO-DATA',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>69,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'M',
            'usecase_id'=>'M01',
            'name'=>'CV Collection',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>70,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'M',
            'usecase_id'=>'M02',
            'name'=>'Update BIO-DATA',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>71,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'M',
            'usecase_id'=>'M03',
            'name'=>'View BIO-DATA',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>72,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'M',
            'usecase_id'=>'M04',
            'name'=>'BIO-DATA Report',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>73,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'N',
            'usecase_id'=>'N00',
            'name'=>'Doctor Report',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>74,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'N',
            'usecase_id'=>'N01',
            'name'=>'Service Performed By Doctor',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>75,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'N',
            'usecase_id'=>'N02',
            'name'=>'Refer Doctor Investigation Advice(OPD)',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>76,
            'company_id'=>1,
            'menu_type'=>'M',
            'menu_id'=>'O',
            'usecase_id'=>'O00',
            'name'=>'Food',
            'status'=>1,
            'created_at'=>'2019-01-27 10:55:22',
            'updated_at'=>'2019-01-27 10:55:22'
        ] );
        Usecase::create( [
            'id'=>77,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'O',
            'usecase_id'=>'O01',
            'name'=>'Monthly Food Charge',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>78,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'O',
            'usecase_id'=>'O02',
            'name'=>'Approve Food Charge',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        Usecase::create( [
            'id'=>79,
            'company_id'=>1,
            'menu_type'=>'S',
            'menu_id'=>'O',
            'usecase_id'=>'O03',
            'name'=>'Print Food Charge',
            'status'=>1,
            'created_at'=>'2019-01-27 10:57:04',
            'updated_at'=>'2019-01-27 10:57:04'
        ] );
        
    }
}

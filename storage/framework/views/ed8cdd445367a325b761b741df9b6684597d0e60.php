<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Overtime Information</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>

    <link href="<?php echo asset('assets/css/jquery.datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery.datetimepicker.js'); ?>"></script>


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    

                    <div class="card-body">
                        <form method="get" action="<?php echo e(route('overtime/dateRangeReportIndex')); ?>" >
                            <?php echo csrf_field(); ?>

                            <div class="form-group row">
                                <label for="from_date" class="col-md-4 col-form-label text-md-right">From Date</label>
                                <div class="col-md-6">
                                    <input type="text" name="from_date" id="from_date" class="form-control" value="<?php echo \Carbon\Carbon::now()->format('d-m-Y'); ?>" required readonly />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="to_date" class="col-md-4 col-form-label text-md-right">To Date</label>
                                <div class="col-md-6">
                                    <input type="text" name="to_date" id="to_date" class="form-control" value="<?php echo \Carbon\Carbon::now()->format('d-m-Y'); ?>" required readonly />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="department_id" class="col-md-4 col-form-label text-md-right">Department</label>
                                <div class="col-md-6">
                                    <?php echo Form::select('department_id',$departments,null,['id'=>'department_id', 'class'=>'form-control']); ?>

                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary" name="action" value="preview">Preview</button>
                                </div>
                                <div class="col-md-3 text-md-center">
                                    <button type="submit" class="btn btn-secondary" name="action" value="print">Print</button>
                                </div>

                                <div class="col-md-4 text-md-right">
                                    <button type="submit" class="btn btn-info" name="action" value="excel">Excel</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <?php if(!empty($data)): ?>

            
            
                
                    
                        
                               
                               
                               

                        

                        
                        
                        
                        
                        
                        
                        
                    
                
            
            



            <?php $__currentLoopData = $dates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$date): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php if($data->contains('ot_date',$date)): ?>
                <div class="card">

                    <div class="card-header">
                        <h3 style="font-weight: bold">Department Name : <?php echo $dept_data->name; ?><br/>
                            Report Title: Overtime Setup Report For Date : <?php echo \Carbon\Carbon::parse($date)->format('d-M-Y'); ?></h3>
                    </div>
                    <div class="card-body">

                        <table class="table table-info table-striped table-bordered">

                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Hour</th>
                                <th>Reason</th>
                                <th>Entered By</th>
                                <th>Approved By</th>

                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($date == $row->ot_date): ?>
                                        <tr>
                                            <td><?php echo $row->employee_id; ?></td>
                                            <td><?php echo $row->professional->personal->full_name; ?></td>
                                            <td><?php echo $row->ot_hour; ?></td>
                                            <td><?php echo $row->reason; ?></td>
                                            <td><?php echo $row->user->name; ?></td>
                                            <td><?php echo $row->approver->name ?? ''; ?></td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

    </div> <!--/.Container-->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>

        $(document).ready(function(){

            $( "#from_date" ).datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false
            });

            $( "#to_date" ).datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false
            });
        });

    </script>


<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
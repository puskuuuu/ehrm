<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Notice Information</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>

    <link href="<?php echo asset('assets/css/jquery.datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery.datetimepicker.js'); ?>"></script>


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <button type="button" class="btn btn-notice btn-success" data-toggle="modal" data-target="#modal-new-notice"><i class="fa fa-plus"></i>New</button>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12" style="overflow-x:auto;">
                <table class="table table-bordered table-hover table-striped" id="notices-table">
                    <thead style="background-color: #b0b0b0">
                    <tr>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Sender</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div> <!--/.Container-->

    <?php echo $__env->make('notice.modals.new-notice-form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('notice.modals.notice-update-form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('notice.modals.notice-file-upload-form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>
        $(function() {
            var table= $('#notices-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                responsive: true,
                ajax: 'noticeDataTable',
                columns: [
                    { data: 'notice_date', name: 'notice_date' },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'sender', name: 'sender' },
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action', orderable: false, searchable: false, printable: false}
                ]
            });
            $(this).on("click", ".btn-notice-edit", function (e) {
                e.preventDefault();

                var data_id = $(this).data('rowid');
                var data_notice_date = $(this).data('notice_date');
                var data_title = $(this).data('title');
                var data_description = $(this).data('description');
                var data_sender = $(this).data('sender');

                document.getElementById('id-for-update').value=data_id;
                document.getElementById('notice_date-for-update').value=data_notice_date;
                document.getElementById('title-for-update').value=data_shortname;
                document.getElementById('description-for-update').value=$(this).data('code');
                document.getElementById('sender-for-update').value=$(this).data('top');
                document.getElementById('email-for-update').value=data_email;
                document.getElementById('description-for-update').value=data_description;



                var str = $(this).data('leave');


                str.toString().substr(0,1) == 1 ? $("#apply").prop("checked", true) : $("#apply").prop("checked", false);
                str.toString().substr(1,1) == 1 ? $("#acknowledge").prop("checked", true) : $("#acknowledge").prop("checked", false);
                str.toString().substr(2,1) == 1 ? $("#recommend").prop("checked", true) : $("#recommend").prop("checked", false);
                str.toString().substr(3,1) == 1 ? $("#approve").prop("checked", true) : $("#approve").prop("checked", false);



            });
            $(this).on("click", ".btn-file-upload", function (e) {
                e.preventDefault();

                document.getElementById('id-for-update').value=$(this).data('rowid');
            });


            // $("body").on("click", ".btn-create", function (e) {
            //     e.preventDefault();
            //
            //     var url = $(this).data('remote');
            //     window.location.href = url;
            //
            // });

            $(this).on("click", ".btn-file-view", function (e) {
                e.preventDefault();

                window.location.href = $(this).data('remote');

            });



        });

        $(function (){
            $(document).on("focus", "input:text", function() {
                $(this).select();
            });
        });

    </script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Investigation Advice Report</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('assets/js/bootstrap3-typeahead.js'); ?>"></script>

    <link href="<?php echo asset('assets/css/jquery.datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery.datetimepicker.js'); ?>"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>
        </div>


        
        
        
        
        
        



        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    
                    
                    <div class="card-body">

                        <form id="search-form-date" method="get" action="<?php echo e(route('doctorReport/referDoctorServiceIndex')); ?>">


                            <div class="form-group row">
                                <label for="emp_id" id="doctor_label" class="col-md-4 col-form-label text-md-right">Doctor Name</label>

                                <div class="col-md-6">

                                    <input id="d_name" type="text" class="form-control typeahead" name="d_name" autocomplete="off">
                                    <input id="doctor_id" type="hidden"  name="doctor_id" required>

                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="from_date" class="col-md-4 col-form-label text-md-right">From Date</label>
                                <div class="col-md-6">
                                    <input type="text" name="from_date" id="from_date" class="form-control" value="<?php echo \Carbon\Carbon::now()->format('d-m-Y'); ?>" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="to_date" class="col-md-4 col-form-label text-md-right">To</label>
                                <div class="col-md-6">
                                    <input type="text" name="to_date" id="to_date" class="form-control" value="<?php echo \Carbon\Carbon::now()->format('d-m-Y'); ?>" required />
                                </div>
                            </div>

                            


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-1">
                                    <button type="submit" class="btn btn-primary" name="action" value="preview">Preview</button>
                                </div>
                                <div class="col-md-5 text-md-right">
                                    <button type="submit" class="btn btn-secondary" name="action" value="print">Print</button>
                                </div>
                            </div>

                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        
        
        
        

        
        

        

        
        
        
        
        
        
        
        

        
        
        
        
        
        
        
        
        

        
        
        
        

        

        

        
        

        
        

        

        
        
        
        
        
        
        
        

        
        
        
        
        
        
        
        
        

        
        
        


        

        
        
    </div> <!--/.Container-->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>



        $(document).ready(function(){

            $( "#from_date").datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false,
            });

            $( "#to_date").datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false,
            });

            $('.summernote').summernote({
                placeholder: 'Descriptions',
                tabsize: 2,
                height: 100
            });



            var autocomplete_path = "<?php echo e(url('autocomplete/refDoctors')); ?>";

            $(document).on('click', '.form-control.typeahead', function() {

                $(this).typeahead({
                    minLength: 2,
                    displayText:function (data) {
                        return data.ref_doctor_name;
                    },
                    source: function (query, process) {
                        $.ajax({
                            url: autocomplete_path,
                            type: 'GET',
                            dataType: 'JSON',
                            data: 'query=' + query ,
                            success: function(data) {
                                return process(data);
                            }
                        });
                    },
                    afterSelect: function (data) {

                        document.getElementById('doctor_id').value = data.ref_doctor_code;

                    }
                });
            });


        });



    </script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
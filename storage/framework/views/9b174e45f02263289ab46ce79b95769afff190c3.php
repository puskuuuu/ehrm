<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Employee Attendance Process</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>

    <link href="<?php echo asset('assets/css/jquery.datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery.datetimepicker.js'); ?>"></script>


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    

                    <div class="card-body">
                        <form method="get" action="<?php echo e(route('attendance/dailyAttendanceStatusIndex')); ?>" >
                            <?php echo csrf_field(); ?>

                            <div class="form-group row">
                                <label for="report_date" class="col-md-4 col-form-label text-md-right">Report Date</label>

                                <div class="col-md-6">

                                    <input type="text" name="report_date" id="report_date" class="form-control" value="<?php echo \Carbon\Carbon::now()->format('d-m-Y'); ?>" required readonly />

                                </div>
                            </div>

                            
                                
                                
                                    
                                
                            



                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-1">
                                    <button type="submit" class="btn btn-primary" name="action" value="preview">Preview</button>
                                </div>
                                <div class="col-md-5 text-md-right">
                                    <button type="submit" class="btn btn-secondary" name="action" value="print">Print</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <?php if(!empty($data)): ?>
            <table class="table table-info table-striped" width="50%">

                <tbody>
                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>Total Employee</td>
                        <td>:</td>
                        <td><?php echo $row->emp_count; ?></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo url('attendance/daily/'.'present'.'/'.$report_date); ?>">
                                Present
                            </a></td>
                        <td>:</td>
                        <td><?php echo $row->present; ?></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo url('attendance/daily/'.'offday'.'/'.$report_date); ?>">
                                Weekly Off Day
                            </a></td>
                        <td>:</td>
                        <td><?php echo $row->offday; ?></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo url('attendance/daily/'.'leave'.'/'.$report_date); ?>">
                                In Leave
                            </a></td>
                        <td>:</td>
                        <td><?php echo $row->n_leave; ?></td>
                    </tr>
                    <tr>
                        <td>In Public Holiday</td>
                        <td>:</td>
                        <td><?php echo $row->holiday; ?></td>
                    </tr>
                    <tr>
                        <td>Next Roster</td>
                        <td>:</td>
                        <td><?php echo $row->next_roster; ?></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo url('attendance/daily/'.'absent'.'/'.$report_date); ?>">
                                Total Absent
                            </a></td>
                        <td>:</td>
                        <td><?php echo $row->absent; ?></td>
                    </tr>


                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>

            </table>

        <?php endif; ?>

    </div> <!--/.Container-->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>

        $(document).ready(function(){

            $( "#report_date" ).datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false
            });
        });

    </script>


<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<span class="heading" style="font-weight: bold; color: #980000">EXTERNAL</span>
<li><a class="font-weight-bold" href="#externalDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-grid"></i>BIO DATA</a>
    <ul id="externalDropdown" class="collapse list-unstyled ">
        <li><a href="{!! route('bioData/biodataCollectionIndex') !!}">CV Collection</a></li>
        <li><a href="{!! route('bioData/updateIndex') !!}">Biodata Update</a></li>
        <li><a href="{!! route('bioData/searchIndex') !!}">Biodata View</a></li>
        <li><a href="#">Report</a></li>

    </ul>
</li>
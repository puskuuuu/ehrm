<?php $__env->startSection('pagetitle'); ?>
    <h2 class="no-margin-bottom">Division Information</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery-3.3.1.min.js'); ?>"></script>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo URL::previous(); ?>"> <i class="fa fa-list"></i> Back </a>
                </div>
            </div>
        </div>


        <div class="card text-center bg-light mb-3" style="width: 60rem;">
            <div class="card-header">
                Biodata Details
            </div>
            <div class="card-body">
                <form action="<?php echo url('bioData/save'); ?>" id="bio-data-form"  method="post" accept-charset="utf-8">
                    <?php echo e(csrf_field()); ?>



                    <div class="form-group row required">
                        <label for="name" class="col-sm-3 col-form-label text-md-right">Name</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="name" id="name" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row required">
                        <label for="mobile_no" class="col-sm-3 col-form-label text-md-right">Mobile No</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="mobile_no" id="mobile_no" class="form-control" required autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row required">
                        <label for="applied_post" class="col-sm-3 col-form-label text-md-right">Applied Post</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="applied_post" id="applied_post" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="speciality" class="col-sm-3 col-form-label text-md-right">Speciality</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="speciality" id="speciality" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row required">
                        <label for="submission_date" class="col-sm-3 col-form-label text-md-right">Submission Date</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="submission_date" id="submission_date" value="<?php echo \Carbon\Carbon::now()->format('d-m-Y'); ?>" class="form-control" required readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="reference_name" class="col-sm-3 col-form-label text-md-right">Reference Name</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <input type="text" name="reference_name" id="reference_name" class="form-control">
                            </div>
                        </div>
                    </div>

                    
                        
                        
                            
                                
                            
                        
                    

                    
                        
                        
                            
                                
                            
                        
                    


                    
                        
                        
                            
                                
                            
                        
                    


                    <div class="form-group row">
                        <label for="remarks" class="col-sm-3 col-form-label text-md-right">Remarks</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-3">
                                <textarea class="form-control" name="remarks" cols="50" rows="4" id="remarks"></textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="card-footer text-muted">

            </div>
        </div>


        <?php if(!empty($data)): ?>

            <div class="card text-center bg-light mb-3" style="width: 60rem;">
                <div class="card-header">
                    Biodata Details
                </div>
                <div class="card-body">

                    <table class="table table-bordered table-success table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Issue No</th>
                                <th>Name</th>
                                <th>Mobile No</th>
                                <th>Applied Post</th>
                                <th>Submission Date</th>
                            </tr>
                        </thead>

                        <tbody>
                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo $row->issue_number; ?></td>
                                <td><?php echo $row->name; ?></td>
                                <td><?php echo $row->mobile_no; ?></td>
                                <td><?php echo $row->applied_post; ?></td>
                                <td><?php echo $row->submission_date; ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>

                    </table>

                </div>
            </div>


        <?php endif; ?>


    </div> <!--/.Container-->






<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

    <script>

        $( function() {

            $( "#submission_date" ).datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false
            });

            $( "#joining_date" ).datetimepicker({
                format:'d-m-Y',
                timepicker: false,
                closeOnDateSelect: true,
                scrollInput : false,
                inline:false
            });



        } );


        $(function (){
            $(document).on("focus", "input:text", function() {
                $(this).select();
            });
        });

    </script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
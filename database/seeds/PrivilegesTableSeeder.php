<?php

use Illuminate\Database\Seeder;

class PrivilegesTableSeeder extends Seeder{

    public function run(){
        
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 1,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 1,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 1,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 1,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 2,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 2,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 2,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 2,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 3,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 3,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 3,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 3,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 4,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 4,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 4,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 4,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 5,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 5,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 5,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 5,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 6,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 6,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 6,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 6,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 7,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 7,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 7,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 7,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 8,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 8,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 8,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 8,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 9,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 9,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 9,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 9,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 10,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 10,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 10,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 10,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 11,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 11,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 11,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 11,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 12,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 12,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 12,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 12,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 13,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 13,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 13,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 13,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 14,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 14,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 14,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 14,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 15,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 15,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 15,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 15,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 16,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 16,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 16,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 16,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 17,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 17,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 17,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 17,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 18,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 18,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 18,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 18,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 19,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 19,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 19,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 19,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 20,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 20,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 20,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 20,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 21,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 21,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 21,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 21,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 22,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 22,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 22,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 22,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 23,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 23,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 23,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 23,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 24,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 24,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 24,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 24,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 25,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 25,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 25,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 25,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 26,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 26,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 26,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 26,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 27,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 27,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 27,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 27,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 28,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 28,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 28,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 28,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 29,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 29,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 29,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 29,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 30,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 30,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 30,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 30,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 31,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 31,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 31,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 31,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);

        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 32,
            'group_id'=>1,
            'add' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 32,
            'group_id'=>1,
            'edit' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 32,
            'group_id'=>1,
            'delete' => 1,
            'approver_id' => 1
        ]);
        DB::table('privileges')->insert([
            'company_id' => 1,
            'user_id' => 1,
            'menu_id' => 32,
            'group_id'=>1,
            'view' => 1,
            'approver_id' => 1
        ]);



DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 33,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 33,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 33,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 33,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 34,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 34,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 34,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 34,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 35,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 35,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 35,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 35,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 36,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 36,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 36,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 36,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 37,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 37,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 37,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 37,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 38,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 38,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 38,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 38,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 39,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 39,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 39,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 39,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 40,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 40,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 40,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 40,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 41,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 41,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 41,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 41,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 42,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 42,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 42,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 42,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 43,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 43,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 43,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 43,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 45,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 45,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 45,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 45,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 46,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 46,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 46,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 46,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 47,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 47,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 47,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 47,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 48,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 48,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 48,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 48,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 49,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 49,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 49,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 49,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 50,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 50,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 50,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 50,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 51,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 51,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 51,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 51,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 52,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 52,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 52,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 52,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 53,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 53,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 53,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 53,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 54,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 54,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 54,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 54,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 55,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 55,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 55,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 55,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 56,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 56,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 56,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 56,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 57,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 57,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 57,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 57,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 58,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 58,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 58,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 58,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 59,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 59,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 59,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 59,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 60,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 60,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 60,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 60,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 61,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 61,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 61,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 61,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 62,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 62,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 62,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 62,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 63,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 63,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 63,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 63,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 64,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 64,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 64,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 64,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 65,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 65,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 65,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 65,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 66,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 66,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 66,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 66,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 67,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 67,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 67,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 67,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 68,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 68,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 68,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 68,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 69,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 69,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 69,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 69,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 70,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 70,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 70,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 70,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 71,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 71,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 71,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 71,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 72,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 72,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 72,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 72,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 73,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 73,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 73,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 73,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 74,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 74,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 74,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 74,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 75,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 75,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 75,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 75,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 76,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 76,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 76,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 76,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 77,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 77,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 77,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 77,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 78,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 78,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 78,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 78,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 79,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 79,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 79,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 79,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 80,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 80,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 80,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 80,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 81,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 81,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 81,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 81,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 82,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 82,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 82,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 82,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 44,
    'group_id'=>1,
    'view' => 1,
    'approver_id' => 1
        ]);

DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 44,
    'group_id'=>1,
    'add' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 44,
    'group_id'=>1,
    'edit' => 1,
    'approver_id' => 1
        ]);
DB::table('privileges')->insert([
    'company_id' => 1,
    'user_id' => 1,
    'menu_id' => 44,
    'group_id'=>1,
    'delete' => 1,
    'approver_id' => 1
        ]);

    }
    
}
